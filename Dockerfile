# syntax=docker/dockerfile:1

FROM python:3.12-slim as base
WORKDIR /app

ENV POETRY_HOME=/poetry \
    PYTHONUNBUFFERED=1
ENV PATH="$POETRY_HOME/bin:$PATH"

RUN --mount=type=cache,target=/root/.cache \
    python3 -m venv /poetry && /poetry/bin/pip install poetry

COPY poetry.lock pyproject.toml ./

RUN --mount=type=cache,target=/root/.cache \
    poetry install --no-root --no-directory --only main

COPY veiltail/ ./
RUN --mount=type=cache,target=/root/.cache \
    poetry install --compile --only main

CMD ["python3", "-m", "veiltail"]
